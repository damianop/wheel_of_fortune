var board = $('.js-board');
var letterExample = $('.js-letter');
var wrongGuess = 0;
var maxWrongGuess = 4;

function drawLetter(letter) {
    var newLetter = letterExample.clone();
    newLetter.text(letter);
    if (letter == ' ') {
        newLetter.addClass('space');
    }
    board.append(newLetter);
}

function createSolution(solution) {
    board.empty();
    var splittedSolution = solution.split('');
    for (var i = 0; i < splittedSolution.length; i++) {
        drawLetter(currentSolution[i]);
    }
}

function addMessage(msg) {
    $('.js-status').append(msg);
    $('.js-status').append($('<br>'));
}

function pickSolution(data) {
    var dataCount = data.length;
    var randomIndex = Math.floor(dataCount * Math.random());
    return data[randomIndex].toLowerCase();
}


function checkLetter(letter) {
    var letters = $('.board').children('.js-letter');
    
    var guessed = false;
    
    letters.each(function () {

        if ($(this).text() === letter) {
            
            $(this).removeClass('folded');
            guessed = true;
        }
    });
    
    if(guessed == false) {
        wrongGuess++;
        if(wrongGuess >= maxWrongGuess) {
            addMessage('przegrałeś');
        }
    }
    
}



var solutions = ["Raz kozie śmierć", "Dla chcącego nic trudnego"];

var currentSolution = pickSolution(solutions);
createSolution(currentSolution);

$('.js-input').on('keyup', function () {
    var guessLetter = $(this).val().toLowerCase();
    $(this).val('');
    checkLetter(guessLetter);

});
